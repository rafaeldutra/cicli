'use strict'

var AdmZip = require('adm-zip');

module.exports = {
    unzipTo : function (file, dstPath, mantainEntryPath) {
        mantainEntryPath = (typeof mantainEntryPath !== 'undefined')? mantainEntryPath: false;
        var zip = new AdmZip(file);
        var entry = zip.getEntries();
        zip.extractEntryTo(entry[0], dstPath, mantainEntryPath, true);
    }
}
