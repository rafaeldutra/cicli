'use strict';

var chalk = require('chalk');


module.exports = {
    print : function(msg){
        console.log("\n" + chalk.blue.bold(`[ Codeigniter CLI ]: \n`) + msg + "\n");
    },
    error : function(msg){
        console.log("\n" + chalk.blue.bold(`[ Codeigniter CLI ]: \n`) + chalk.red(msg) + "\n");
    },
    success : function(msg){
        console.log("\n" + chalk.blue.bold(`[ Codeigniter CLI ]: \n`) + chalk.green(msg) + "\n");
    },
    command : function(msg){
        console.log(chalk.yellow.bold(msg) + "\n");
    }
}
