'use strict'

const util = require('util');
const EventEmitter = require('events').EventEmitter;

const fs = require('fs');
const https = require('https');

function Download(url) {
    this.url = url;
}

util.inherits(Download, EventEmitter);

Download.prototype.downTo = function (dest){
    var self = this;
    try {
        var file = fs.createWriteStream(dest);
        https.get(this.url, function(res) {
            var dataLen = 0;
            var contentLen = (res.headers['content-length'])?res.headers['content-length']:-1;
            contentLen = parseInt(contentLen, 10);
            res.on('data', function(chunk){
                dataLen += chunk.length;
                file.write(chunk);
                self.emit('progress', { progress: dataLen, size: contentLen });
            });
            file.on('finish', function(){
                self.emit('completed', true);
            });
            res.on('end', function(){
                file.end();
            });
            res.on('error', function(e) {
                self.emit('error', e);
            });
        });
    } catch (err) {
        this.emit('error', err);
    }
}

module.exports = Download;
