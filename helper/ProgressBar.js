'use strict';

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const chalk = require('chalk');

function ProgressBar(title) {
    this.title = title;
    this.printTitle = true;
}

ProgressBar.prototype.setProgress = function (progress, size) {
    if(this.printTitle) {
        this.printTitle = false;
        console.log("\n" + chalk.blue.bold(`[ Codeigniter CLI ]:`));
        console.log(chalk.blue(this.title) + '\n');
    }

    var already = progress/size;
    var ttySize = process.stdout.columns;

    ttySize = ttySize - 10;
    if(ttySize < 15) ttySize = 15;

    rl.write(null, { ctrl: true, name: 'u' });


    rl.write(chalk.yellow(`${Math.trunc(already*100)}% `));
    rl.write(chalk.yellow.bgYellow(Array(Math.trunc(already * ttySize)).join('=')));
    rl.write(chalk.white.bgWhite(Array(Math.trunc((1-already) * ttySize)).join('=')));
    if(already >= 1) {
        rl.write('\n');
        rl.close();
        return;
    }
}
module.exports = ProgressBar;
