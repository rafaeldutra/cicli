#! /usr/bin/env node
'use strict';

var path = require('path');

// global vars
global.__appRoot = path.resolve(__dirname);
require(__appRoot + '/system/global');
require(__appRoot + '/system/route');
