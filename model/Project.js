'use strict';

var fs = require('fs');
var path = require('path');
var projectsData = fs.readFileSync(__projectsDB);
var projectDB = JSON.parse(projectsData);

function Project() {
    this.absolutePath = projectDB.current.absolutePath;
    this.applicationPath = projectDB.current.applicationPath;
    this.files = projectDB.current.files;
}
Project.prototype = {
    readFiles : function () {
        this.applicationPath = path.join(this.absolutePath, 'application');
        this.files['autoload'] = path.join(this.applicationPath, 'config','autoload.php');
        if (!fs.lstatSync(this.applicationPath).isDirectory()) throw "'Application' path not found in: " + this.absolutePath;
        if (!fs.lstatSync(this.files['autoload']).isFile()) throw "'Autoload' file not found in: " + this.files['autoload'];
    },
    save : function () {
        var projectsData = fs.readFileSync(__projectsDB);
        var projectDB = JSON.parse(projectsData);
        var projectExists = false;
        projectDB.current = this;
        for(let project of projectDB.projects) {
            if (project.absolutePath == this.absolutePath) {
                projectExists = true;
                break;
            }
        }
        if(!projectExists) projectDB.projects.push(this);
        projectsData = JSON.stringify(projectDB);
        fs.writeFile(__projectsDB, projectsData, function (e) {
            if (e) throw e.message;
        });
    },
    getCurrent : function () {
        var projectsData = fs.readFileSync(__projectsDB);
        var projectDB = JSON.parse(projectsData);
        return projectDB.current.absolutePath;
    }
}
module.exports = new Project();
