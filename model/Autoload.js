'use strict';

var fs = require('fs');
var projectsData = fs.readFileSync(__projectsDB);
var projectDB = JSON.parse(projectsData);

function Autoload() {
    this.curProject = projectDB.current;
}
Autoload.prototype = {
    show : function(system){
        var regexp = getRePhpArray(system);
        var lines = getLines(this.curProject);
        var props;
        for (let line of lines) {
            if(line.match(/^\|/)) continue;
            if(regexp.test(line)) {
                props = getProperties(line);
                break;
            }
        }
        return Array.from(props);
    },
    add: function(system, props) {
        var regexp = getRePhpArray(system);
        var lines = getLines(this.curProject);
        var notFound = true;
        for (let key in lines) {
            if(lines[key].match(/^\|/)) continue;
            if(regexp.test(lines[key])) {
                lines[key] = updateLineAdd(lines[key], props);
                notFound = false;
                break;
            }
        }
        if(notFound) throw Error(system + ' not found in "autoload.php".');
        fs.writeFileSync(this.curProject.files['autoload'], lines.join('\n'));
    },
    remove : function (system, props) {
        var regexp = getRePhpArray(system);
        var lines = getLines(this.curProject);
        var notFound = true;
        for (let key in lines) {
            if(lines[key].match(/^\|/)) continue;
            if(regexp.test(lines[key])) {
                lines[key] = updateLineRemove(lines[key], props);
                notFound = false;
                break;
            }
        }
        if(notFound) throw system + ' not found in "autoload.php".';
        fs.writeFileSync(this.curProject.files['autoload'], lines.join('\n'));
    }
}
module.exports = new Autoload();

// Funções para panipular texto

// Recebe uma linha do arquivo ($autoload['example'] = array('prop1','prop2'))
// e retorna um array js com propN's
function getProperties(line) {
    var prop = /\'\w*\'/g;
    return new Set(line.split("=")[1].match(prop));
}
// Atualiza propriedades de linha do arquivo ($autoload['example'] = array('prop1','prop2'))
function setProperties(line, properties) {
    return line.split("=")[0].trim() + " = array("+ properties.join(",") +");";
}

// Funções auxiliares
function updateLineAdd(line, addProperties){
    var addProperties = new Set(addProperties);
    var regex = /\'\w*\'/i;
    var oldProperties = getProperties(line);
    addProperties.forEach(function (value) {
        if (!regex.test(value)) value = '\''+ value + '\'';
        oldProperties.add(value);
    });
    return setProperties(line, Array.from(oldProperties));
}

function updateLineRemove(line, rmProperties){
    var rmProperties = new Set(rmProperties);
    var regex = /\'\w*\'/i;
    var oldProprerties = getProperties(line);
    rmProperties.forEach(function(value) {
        if(!regex.test(value)) value = '\''+ value + '\'';
        oldProprerties.delete(value);
    });
    return setProperties(line, Array.from(oldProprerties));
}

function getLines(project) {
    return fs.readFileSync(project.files['autoload']).toString().split("\n");
}

function getRePhpArray (system){
    return new RegExp("\\$autoload\\[[\',\"]" + system + "[\',\"]\\]\\s*=\\s*array(\\w*)");
}
