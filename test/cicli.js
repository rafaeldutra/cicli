// 'use strict';

var exec = require('child_process').exec;
var pkg = require('../package.json');
var cicli = './cicli.js';
var expect = require('chai').expect;

describe('Cicle', function(){
    it('Should return version of cicle', function(done){
        exec(cicli + ' --version', function(err, stdout,stderr){
            if(err) throw err;
            expect(stdout.replace('\n','')).to.equal("cicli " + pkg.version);
            done();
        });
    });
});
