'use strict';

var os = require('os');
var fs = require('fs');

global.__homePath = os.homedir() + '/.cicli';
global.__dataPath = __homePath + '/data';
global.__ciPath = __homePath + '/ci';
global.__projectsDB = __dataPath + '/projects.json';

if(!fs.existsSync(__ciPath)) fs.mkdirSync(__ciPath);
if(!fs.existsSync(__dataPath)) {
    fs.mkdirSync(__dataPath);
    fs.writeFileSync(__projectsDB,JSON.stringify({"current":{},"projects":[]}),'utf8');
}
global.requireModel = function(model) {
    return require(__appRoot + '/model/' + model);
}
global.requireController = function(controller) {
    return require(__appRoot + '/controller/' + controller);
}
global.requireHelper = function(helper) {
    return require(__appRoot + '/helper/' + helper);
}
