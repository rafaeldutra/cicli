'use strict';
var router = require('route-cli');
var Project = requireController('Project');
var view = requireHelper('viewCli');
var Autoload = requireController('Autoload');

// commands to Project
router.route(['project', 'add'], function(command, additionalCommands, flags){
    if(flags[2] && flags[2].startsWith('/')) additionalCommands.push(flags[2]);
    Project.add(additionalCommands);
});
// router.route(['project', 'remove'], function(command, additionalCommands, flags){
//
// });
// router.route(['project', 'use'], function(command, additionalCommands, flags){
//
// });
router.route(['project', 'new'], function(command, additionalCommands, flags){
    if(flags[2] && flags[2].startsWith('/')) additionalCommands.push(flags[2]);
    Project.new(additionalCommands[0]);
});
router.route(['project', 'current'], function(command, additionalCommands, flags){
    Project.showCurrent();
});


// commands to Autoload
router.route(['autoload', 'add'], function(command, additionalCommands, flags){
    Autoload.add(additionalCommands.shift(),additionalCommands);
});
router.route(['autoload', 'show'], function(command, additionalCommands, flags){
    Autoload.show(additionalCommands[0]);
});
router.route(['autoload', 'remove'], function(command, additionalCommands, flags){
    Autoload.remove(additionalCommands.shift(),additionalCommands);
});

// default router
router.default(function(command, additionalCommands, flags){
    view.error("Command not found - cicli " +  additionalCommands.join(' '));
});

// start router
try {
    router.execute(process.argv);
} catch(e){
  view.error(e);
}
