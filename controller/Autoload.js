'use strict';

var Autoload = requireModel("Autoload");
var view = requireHelper('viewCli');

module.exports = {
    add : function (system, param) {
        try {
            Autoload.add(system, param);
            view.success('Parameter [ ' + param.join(' , ') + ' ] has been added in the ' + system + '.');
        } catch(e) {
            view.error(e);
        }
    },
    remove : function (system, param) {
        try {
            Autoload.remove(system, param);
            view.success('Parameter [ ' + param.join(' , ') + ' ] has been removed of ' + system + '.');
        } catch(e) {
            view.error(e);
        }
    },
    show : function (system) {
        try {
            var params = Autoload.show(system);
            view.success('Autoload - ' + system + ': [' + params.toString() + ']');
        } catch(e) {
            view.error(e);
        }

    }
}
