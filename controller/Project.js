'use strict';

var Project = requireModel('Project');
var view = requireHelper('viewCli');
var Download = requireHelper('Download');
var ProgressBar = requireHelper('ProgressBar');
var file = requireHelper('files');
const path = require('path');
const fs = require('fs');

module.exports = {
    add : function (projectPath) {
        if (projectPath.length == 0) {
            projectPath = process.cwd();
        } else projectPath = projectPath[0];
        Project.absolutePath = path.isAbsolute(projectPath)? projectPath: path.join(process.cwd(), projectPath);
        try {
            Project.readFiles();
            Project.save();
            view.success(
                'Novo projeto adicionado e configurado como corrente:\n' +
                'Caso queira alternar para outro projeto entre com:\n'
            );
            view.command('> cicli project use <pastaDoProjeto>\n');
        } catch (e) {
            view.error(e);
        }
    },

    // Recebe nome (caminho absluto ou relativo) para novo project
    // Escolhe versão do CodeIgniter quer será utilizada (Baixa caso não existe)
    //
    new : function (name){
        try {
            //Trata o caminho para o novo projeto
            var folder = (path.isAbsolute(name))?name:path.join('.', name);
            if(!fs.existsSync(folder)) fs.mkdirSync(folder);
            var ci = { url : 'https://codeload.github.com/bcit-ci/CodeIgniter/zip/3.0.6', vs : '3-0-6', ext : '.zip' };
            var ciStored = path.join( __ciPath, (ci.vs + ci.ext));

            //Caso não exite no storange faz o download do arquivo zip
            if (!fs.existsSync(ciStored)) {
                var NewCi = new Download(ci.url);
                var Bar = new ProgressBar(`Download CI to ${folder}`);

                NewCi.downTo(ciStored);

                NewCi.on('progress', function (res){
                    Bar.setProgress(res.progress, res.size);
                });
                NewCi.on('completed', function (){
                    // Extrai do storange para o local do novo projeto
                    file.unzipTo(ciStored, folder, false);
                });
                NewCi.on('error', function (err){
                    view.error(e);
                });

            }
            // Caso já tenha versão do ci no storange
            // Extrai do storange para o local do novo projeto
            else {
                file.unzipTo(ciStored, folder, false);
            }
        } catch (e) {
            view.error(e);
        }
    },

    // Escreve no console o prejeto de trabalho atual
    showCurrent : function (){
        view.success('Project: ' + Project.getCurrent());
    },
    remove : function (){

    },
    use : function (){

    },
    delete : function (){

    }
}
