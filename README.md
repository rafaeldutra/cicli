# README #

Este projeto tem como objetivo uma aplicação cli escrita com js que manipule os arquivos da framework Codeigniter (PHP).

### Funcionalidades ###

* Criação da estrutura da webapp (Download da framework)
* Alteração rápida dos arquivos de configuração (application/config)
* Criação de scaffold com base na leitura de um banco de dados

### Status do programa ###

* Version 0.0.10 (alpha)
* Criação da estrutura
* Função para addicionar projeto
* Função para ler o arquivo autoloader
